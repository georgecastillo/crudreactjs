import React, { Component } from 'react';
import axios from 'axios';

export default class Edit extends Component {
    constructor(props) {
        super(props);
        this.onChangePersonName = this.onChangePersonName.bind(this)
        this.onChangenDni = this.onChangenDni.bind(this)
        this.onChangeDireccion = this.onChangeDireccion.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

        this.state = {
            nombre: '',
            dni: '',
            direccion: ''
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8080/api/person/' + this.props.match.params.id)
            .then(response => {
                if (response.data.length > 0) {
                    this.setState({
                        nombre: response.data[0].nombre,
                        dni: response.data[0].dni,
                        direccion: response.data[0].direccion
                    })
                }
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onChangePersonName(e) {
        this.setState({
            nombre: e.target.value
        });
    }
    onChangenDni(e) {
        this.setState({
            dni: e.target.value
        })
    }
    onChangeDireccion(e) {
        this.setState({
            direccion: e.target.value
        })
    }

    onSubmit(e) {
        e.preventDefault();
        const obj = {
            nombre: this.state.nombre,
            dni: this.state.dni,
            direccion: this.state.direccion
        };
        axios.put('http://localhost:8080/api/person/' + this.props.match.params.id, obj)
            .then(res => {
                console.log("Edited", res.data)
                this.setState({ showLoader: false })
                this.props.history.push('/index')
            })
            .catch(err => console.log(err))
    }

    render() {
        return (
            <div style={{ marginTop: 10 }}>
                <h3 align="center">Actualizar persona</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Nombre:  </label>
                        <input
                            type="text"
                            className="form-control"
                            value={this.state.nombre}
                            onChange={this.onChangePersonName}
                        />
                    </div>
                    <div className="form-group">
                        <label>DNI: </label>
                        <input type="text"
                            className="form-control"
                            value={this.state.dni}
                            onChange={this.onChangenDni}
                        />
                    </div>
                    <div className="form-group">
                        <label>Dirección: </label>
                        <input type="text"
                            className="form-control"
                            value={this.state.direccion}
                            onChange={this.onChangeDireccion}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit"
                            value="Actualizar persona"
                            className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}
