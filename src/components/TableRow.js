import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class TableRow extends Component {
  constructor(props) {
    super(props);
    this.delete = this.delete.bind(this);
  }

  delete() {
    axios.delete('http://localhost:8080/api/person/' + this.props.obj.id)
      .then((res) => {
        console.log('Deleted', res.data)
        this.props.updateState()
      })
      .catch(err => console.log(err))
  }

  render() {
    return (
      <tr>
        <td>
          {this.props.obj.nombre}
        </td>
        <td>
          {this.props.obj.dni}
        </td>
        <td>
          {this.props.obj.direccion}
        </td>
        <td>
          <Link to={"/edit/" + this.props.obj.id} className="btn btn-primary">Editar</Link>
        </td>
        <td>
          <button onClick={this.delete} className="btn btn-danger">Eliminar</button>
        </td>
      </tr>
    );
  }
}


export default TableRow;
