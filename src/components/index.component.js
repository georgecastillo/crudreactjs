import React, { Component } from 'react'
import axios from 'axios'
import TableRow from './TableRow'

export default class Index extends Component {
    constructor(props) {
        super(props)
        this.state = { persons: [] }
        this.updateState = this.updateState.bind(this)
    }

    updateState = () => {
        axios.get('http://localhost:8080/api/person')
            .then(response => {
                console.log("Listar")
                this.setState({ persons: response.data });
            })
            .catch(function (error) {
                console.log(error.response);
            })
    }

    componentDidMount() {
        this.updateState()
    }

    tabRow() {
        return this.state.persons.map((object, i) => {
            return <TableRow obj={object} updateState={this.updateState} key={i} />
        });
    }

    render() {
        return (
            <div>
                <h3 align="center">Lista de personas</h3>
                <table className="table table-striped" style={{ marginTop: 20 }}>
                    <thead>
                        <tr>
                            <th>Persona</th>
                            <th>DNI</th>
                            <th>Dirección</th>
                            <th colSpan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.tabRow()}
                    </tbody>
                </table>
            </div>
        );
    }
}
