import React, { Component } from 'react';
import axios from 'axios';

export default class Create extends Component {
    constructor(props) {
        super(props)
        this.onChangePersonName = this.onChangePersonName.bind(this)
        this.onChangePersonDni = this.onChangePersonDni.bind(this)
        this.onChangePersonAddress = this.onChangePersonAddress.bind(this)
        this.onSubmit = this.onSubmit.bind(this)

        this.state = {
            person_name: '',
            person_dni: '',
            person_address: ''
        }
    }

    onChangePersonName(event) {
        this.setState({
            person_name: event.target.value
        })
    }

    onChangePersonDni(event) {
        this.setState({
            person_dni: event.target.value
        })
    }

    onChangePersonAddress(event) {
        this.setState({
            person_address: event.target.value
        })
    }

    onSubmit(event) {
        event.preventDefault();
        const obj = {
            person_name: this.state.person_name,
            person_dni: this.state.person_dni,
            person_address: this.state.person_address
        };
        axios.post('http://localhost:8080/api/person/', obj)
            .then(res => console.log("Created",res.data));

        this.setState({
            person_name: '',
            person_dni: '',
            person_address: ''
        })
    }

    render() {
        return (
            <div style={{ marginTop: 10 }}>
                <h3>Agregar nueva persona</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Nombre:</label>
                        <input type="text" className="form-control" value={this.state.person_name} onChange={this.onChangePersonName} />
                    </div>
                    <div className="form-group">
                        <label>DNI:</label>
                        <input type="text" className="form-control" value={this.state.person_dni} onChange={this.onChangePersonDni} />
                    </div>
                    <div className="form-group">
                        <label>Dirección:</label>
                        <input type="text" className="form-control" value={this.state.person_address} onChange={this.onChangePersonAddress} />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Registrar" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}

